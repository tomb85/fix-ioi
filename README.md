# Overview
Compare QuickFixJ and Artio FIX engines from the initiator standpoint. In this example
a `fix-server` is launched which periodically sends `IndicationOfInterest` messages to the 
FIX session. Subsequently, one of the FIX clients will consume the messages.

# Building
To build execute the following from the command line.

```
./gradlew build
```

> This will also generate and compile FIX codes under `fix-codecs` project which are required by the
>`fix-client-artio`.

# Running
You should run components in the following sequence.

## QuickFixJ Client
1. fix-server
2. fix-client-quickfix

## Artio Client
1. fix-server
2. fix-gateway
2. fix-client-artio

> When running the `artio` client it is possible to stop the client and let the gateway keep the FIX session
> alive. After the client comes back on-line it will hijack the session currently owned by the `gateway`.

# Cleanup
To reset the state of the system delete `store-gateway`, `store-server` and `store-client` directories which
are storing sequence numbers and messages for the replay.

# Configuration
Check for `gateway.properties` and `client.properties` if you need to adjust things like `aeron` directories and control
files locations.