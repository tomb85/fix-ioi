package com.dfxtraders.fix.client.quickfix;

import quickfix.*;

import java.util.concurrent.CountDownLatch;

public class Main {

    public static void main(String[] args) throws ConfigError, InterruptedException {

        final var settings = new SessionSettings("quickfix.settings");
        final var storeFactory = new FileStoreFactory(settings);
        final var logFactory = new ScreenLogFactory(settings);
        final var messageFactory = new quickfix.fix42.MessageFactory();
        final var initiator = new SocketInitiator(new ApplicationAdapter(), storeFactory, settings, logFactory, messageFactory);
        initiator.start();

        new CountDownLatch(1).await();
    }
}