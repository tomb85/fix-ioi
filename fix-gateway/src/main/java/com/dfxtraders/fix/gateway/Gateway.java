package com.dfxtraders.fix.gateway;

import com.dfxtraders.fix.codecs.fix42.FixDictionaryImpl;
import io.aeron.CommonContext;
import io.aeron.archive.Archive;
import io.aeron.archive.ArchiveThreadingMode;
import io.aeron.archive.ArchivingMediaDriver;
import io.aeron.driver.MediaDriver;
import io.aeron.driver.ThreadingMode;
import uk.co.real_logic.artio.engine.EngineConfiguration;
import uk.co.real_logic.artio.engine.FixEngine;

public class Gateway {

    public void start() {

        final var configuration = new EngineConfiguration()
                .libraryAeronChannel(CommonContext.IPC_CHANNEL)
                .monitoringFile(System.getProperty("gateway.monitoring.file"))
                .logFileDir(System.getProperty("gateway.log.file"))
                .acceptorfixDictionary(FixDictionaryImpl.class);

        configuration.aeronArchiveContext()
                .controlRequestChannel(CommonContext.IPC_CHANNEL)
                .controlResponseChannel(CommonContext.IPC_CHANNEL);

        final MediaDriver.Context context = new MediaDriver.Context()
                .threadingMode(ThreadingMode.SHARED)
                .dirDeleteOnStart(true);

        final Archive.Context archiveContext = new Archive.Context()
                .threadingMode(ArchiveThreadingMode.SHARED)
                .controlChannel(CommonContext.IPC_CHANNEL)
                .recordingEventsChannel(CommonContext.IPC_CHANNEL);

        ArchivingMediaDriver.launch(context, archiveContext);
        FixEngine.launch(configuration);
    }
}
