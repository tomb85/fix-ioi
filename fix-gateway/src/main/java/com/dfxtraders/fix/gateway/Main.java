package com.dfxtraders.fix.gateway;

import org.agrona.SystemUtil;

import java.util.concurrent.CountDownLatch;

public class Main {

    public static void main(String[] args) throws InterruptedException {
        SystemUtil.loadPropertiesFile("gateway.properties");
        new Gateway().start();
        new CountDownLatch(1).await();
    }
}