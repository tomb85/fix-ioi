package com.dfxtraders.fix.server;

import quickfix.ApplicationAdapter;
import quickfix.Session;
import quickfix.SessionID;

import java.util.concurrent.CompletableFuture;

public class Engine extends ApplicationAdapter {

    private final CompletableFuture<Session> createSession = new CompletableFuture<>();

    public CompletableFuture<Session> createSession() {
        return createSession;
    }

    @Override
    public void onCreate(SessionID sessionId) {
        final var session = Session.lookupSession(sessionId);
        System.err.println("Session created: " + session);
        createSession.complete(session);
    }
}