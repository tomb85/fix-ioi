package com.dfxtraders.fix.server;

import quickfix.*;

public class Main {

    public static void main(String[] args) throws ConfigError {

        final var settings = new SessionSettings("quickfix.settings");
        final var storeFactory = new FileStoreFactory(settings);
        final var logFactory = new ScreenLogFactory(settings);
        final var messageFactory = new quickfix.fix42.MessageFactory();
        final var engine = new Engine();
        final var acceptor = new SocketAcceptor(engine, storeFactory, settings, logFactory, messageFactory);
        acceptor.start();

        final var session = engine.createSession().join();

        final var generator = new Generator(session);
        generator.start();
    }
}
