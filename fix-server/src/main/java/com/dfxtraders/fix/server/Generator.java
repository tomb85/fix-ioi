package com.dfxtraders.fix.server;

import quickfix.DataDictionary;
import quickfix.Session;
import quickfix.SessionNotFound;
import quickfix.field.*;
import quickfix.fix42.IndicationofInterest;
import quickfix.fix42.Message;

import java.util.ArrayList;
import java.util.Random;
import java.util.concurrent.Executors;
import java.util.concurrent.TimeUnit;
import java.util.concurrent.atomic.AtomicLong;

public class Generator {

    private final IndicationofInterest ioi = new IndicationofInterest();
    private final Session session;

    private final AtomicLong ioiIdCounter = new AtomicLong(0);
    private final Random random = new Random();

    public Generator(Session session) {
        this.session = session;
        try {
            final var lastSeqNum = session.getExpectedSenderNum() - 1;
            if (lastSeqNum != 0) {
                final var dictionary = new DataDictionary("FIX42.xml");
                final var messages = new ArrayList<String>();
                session.getStore().get(lastSeqNum, lastSeqNum, messages);
                ioi.fromString(messages.get(0), dictionary, true);
                ioiIdCounter.set(Integer.parseInt(ioi.getIOIID().getValue()) + 1);
            }
        } catch (Exception e) {
            throw new RuntimeException(e);
        } finally {
            ioi.reset();
        }
    }

    public void start() {
        final var es = Executors.newSingleThreadScheduledExecutor();
        es.scheduleAtFixedRate(() -> {
            try {
                Session.sendToTarget(ioi(), session.getSessionID());
            } catch (SessionNotFound sessionNotFound) {
                throw new RuntimeException(sessionNotFound);
            } finally {
                ioi.reset();
            }
        }, 0, 5, TimeUnit.SECONDS);
    }

    private Message ioi() {
        ioi.set(new IOIID(String.valueOf(ioiIdCounter.getAndIncrement())));
        ioi.set(new IOITransType(IOITransType.NEW));
        ioi.set(new Symbol("XRP"));
        ioi.set(new Currency("EUR"));
        ioi.set(new Side(Side.BUY));
        ioi.set(new IOIShares(String.valueOf(Math.max(1, random.nextInt(1000)))));
        return ioi;
    }
}
