package com.dfxtraders.fix.client.artio;

import org.agrona.SystemUtil;

import java.util.concurrent.CountDownLatch;

public class Main {

    public static void main(String[] args) throws InterruptedException {
        SystemUtil.loadPropertiesFile("client.properties");
        new Initiator().start();
        new CountDownLatch(1).await();
    }
}
