package com.dfxtraders.fix.client.artio;

import com.dfxtraders.fix.codecs.fix42.FixDictionaryImpl;
import io.aeron.CommonContext;
import org.agrona.concurrent.IdleStrategy;
import org.agrona.concurrent.SleepingIdleStrategy;
import uk.co.real_logic.artio.library.*;

import java.util.List;

public class Initiator implements LibraryConnectHandler, SessionExistsHandler {

    private final IdleStrategy idleStrategy = new SleepingIdleStrategy();

    private FixLibrary library;

    public void start() {

        final var handler = new IOIHandler();

        final var libraryConfiguration = new LibraryConfiguration()
                .libraryConnectHandler(this)
                .sessionAcquireHandler((session, acquiredInfo) -> handler)
                .sessionExistsHandler(this)
                .libraryAeronChannels(List.of(CommonContext.IPC_CHANNEL));

        libraryConfiguration.defaultHeartbeatIntervalInS(30);

        library = FixLibrary.connect(libraryConfiguration);

        loop();
    }

    private void loop() {
        new Thread(() -> {
            while (true) {
                idleStrategy.idle(library.poll(1));
            }
        }).start();
    }

    @Override
    public void onConnect(FixLibrary library) {

        System.err.println("Library connected");

        final var sessionConfiguration = SessionConfiguration.builder()
                .address("localhost", 8500)
                .targetCompId("DFX_TRADERS")
                .senderCompId("TB")
                .sequenceNumbersPersistent(true)
                .fixDictionary(FixDictionaryImpl.class)
                .build();

        library.initiate(sessionConfiguration);
    }

    @Override
    public void onDisconnect(FixLibrary library) {
        System.err.println("Library disconnected");
    }

    @Override
    public void onSessionExists(FixLibrary library,
                                long surrogateSessionId,
                                String localCompId,
                                String localSubId,
                                String localLocationId,
                                String remoteCompId,
                                String remoteSubId,
                                String remoteLocationId,
                                int logonReceivedSequenceNumber,
                                int logonSequenceIndex) {
        System.err.println("Session exists, hijacking");
        library.requestSession(surrogateSessionId, FixLibrary.NO_MESSAGE_REPLAY, FixLibrary.NO_MESSAGE_REPLAY, 10_000);
    }
}