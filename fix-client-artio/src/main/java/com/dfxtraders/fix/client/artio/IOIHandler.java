package com.dfxtraders.fix.client.artio;

import com.dfxtraders.fix.codecs.fix42.decoder.IOIDecoder;
import io.aeron.logbuffer.ControlledFragmentHandler.Action;
import org.agrona.DirectBuffer;
import uk.co.real_logic.artio.library.OnMessageInfo;
import uk.co.real_logic.artio.library.SessionHandler;
import uk.co.real_logic.artio.messages.DisconnectReason;
import uk.co.real_logic.artio.session.Session;
import uk.co.real_logic.artio.util.AsciiBuffer;
import uk.co.real_logic.artio.util.MutableAsciiBuffer;

public class IOIHandler implements SessionHandler {

    private final IOIDecoder decoder = new IOIDecoder();
    private final AsciiBuffer asciiBuffer = new MutableAsciiBuffer();

    @Override
    public Action onMessage(DirectBuffer buffer,
                            int offset,
                            int length,
                            int libraryId,
                            Session session,
                            int sequenceIndex,
                            long messageType,
                            long timestampInNs,
                            long position,
                            OnMessageInfo messageInfo) {

        if (messageType == IOIDecoder.MESSAGE_TYPE) {
            try {
                asciiBuffer.wrap(buffer, offset, length);
                decoder.decode(asciiBuffer, 0, length);
                System.out.println(decoder.toString());
            } finally {
                decoder.reset();
            }
        }

        return Action.CONTINUE;
    }

    @Override
    public void onTimeout(int libraryId, Session session) {

    }

    @Override
    public void onSlowStatus(int libraryId, Session session, boolean hasBecomeSlow) {

    }

    @Override
    public Action onDisconnect(int libraryId, Session session, DisconnectReason reason) {
        System.err.println("Session disconnected: " + session);
        return Action.CONTINUE;
    }

    @Override
    public void onSessionStart(Session session) {
        System.err.println("Session started: " + session);
    }
}